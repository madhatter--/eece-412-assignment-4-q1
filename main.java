package main;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class main {

	public static void main(String[] args) throws IOException{
		
		// read hashed password from file
		String hashedPassword = getHashedPassword().toLowerCase();
		String hashedMsg;
		String msg;
		String output = "NULL";
		System.out.println("hashedPassword is: " + hashedPassword);
		
		// loop to compute every possible hash value of salt+0000 ~ salt+9999
		int pwdFlag = 0;
		try{
			for(int i=0; i<10000; i++){
				
				msg=Integer.toString(i);
				switch (msg.length()){
					case 1: msg = "000" + msg;
							break;
					case 2: msg = "00" + msg;
							break;
					case 3: msg = "0" + msg;
							break;
					default: 
							break;
				}
				msg = "gS"+msg;
				hashedMsg=sha1(msg);	
				
				// check if computed hash equal to hashed password
				if(hashedMsg.equals(hashedPassword) == true){
					pwdFlag = 1;
					output = msg;
				}
			}
			System.out.println("pwdFlag is: " + pwdFlag);
			if (pwdFlag == 0){
				System.out.println("Password not found.");
			}
			else{
				System.out.println("Found the password: " + output);
			}
		}
		 catch (NoSuchAlgorithmException e) {
		        System.err.println("I'm sorry, but MD5 is not a valid message digest algorithm");
		    }

		return;
	}

// Compute the SHA-1 hash for a string input
	public static String sha1(String input) throws NoSuchAlgorithmException{
		MessageDigest md = MessageDigest.getInstance("SHA1");
		md.reset();
		md.update(input.getBytes());
		byte[] output = md.digest();
		String hex = byteToHex(output);
		return hex;
		
	}	
	
// Convert byte to hex values
	public static String byteToHex(byte[] bytes){
		StringBuilder sb = new StringBuilder(bytes.length * 2);  
		  
	    @SuppressWarnings("resource")
		Formatter formatter = new Formatter(sb);  
	    for (byte b : bytes) {  
	        formatter.format("%02x", b);  
	    }  
	  
	    return sb.toString();  
	}  
		

// Read hashed string from file
	public static String getHashedPassword() throws IOException{
		BufferedReader br = new BufferedReader(new FileReader("pwd1.txt"));
		try{
			String line = br.readLine();
			String password;
			//System.out.println(line+"\n");
			
			while (line!=null){
				if(line.startsWith("g")){
					password = line.substring(2, 42);
					System.out.println("Given hash is: " + password);
					return password;
				}
				else{
				line=br.readLine();
				}
			}
		} finally {
			br.close();
		}
		return null;
	}
	


}
